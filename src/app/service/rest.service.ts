import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  apiUrl: string = environment.apiUrl;
  apiUrl2: string = environment.apiUrl2;
  constructor(private http: HttpClient) { }
  get_faq(): Observable<any> {
    return this.http.get(this.apiUrl2+'faq')
  }

  get_video(data:any): Observable<any> {
    return this.http.get(this.apiUrl+`video-url?search=${data}`)
  }

  add_waitlist(data: any): Observable<any> {
    return this.http.post(this.apiUrl+'waitlist',data)
  }
  schedule_demo(data: any): Observable<any> {
    return this.http.post(this.apiUrl+'scheduledemo',data)
  }

  get_intouch(data:any): Observable<any> {
    return this.http.post(this.apiUrl+'getintouch',data)
  }
}
