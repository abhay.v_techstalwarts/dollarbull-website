import { RestService } from './../service/rest.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-advisors',
  templateUrl: './advisors.component.html',
  styleUrls: ['./advisors.component.scss']
})
export class AdvisorsComponent implements OnInit {
  partnerForm:any;
  videourl:any;
  type:any;
  constructor(private route: ActivatedRoute,private sanitizer: DomSanitizer ,private rest: RestService ,private fb: FormBuilder,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.partnerForm = this.fb.group({
      Name: ['', Validators.required],
      email: ['',Validators.required],
      phone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    });

    this.route.queryParams
      .subscribe(params => {
        console.log(params); // { orderby: "price" }
        this.type = params.type;
        if(this.type == 'contactus'){
          document.getElementById('demoModal-btn')?.click();
        }
      }
    );


    let data = 'Partners';
    this.rest.get_video(data).subscribe(
      success => {  
      if(success.status == 1){
        console.log(success);
        this.videourl = this.sanitizer.bypassSecurityTrustResourceUrl(success.result[0].url);
      }
      },
      error=>{
        console.log("message");
        console.log(error);
        this.toastr.error(error.error);

      }
    
    );
  }


  partnerSubmit(){
      console.log(this.partnerForm.get('Name').value);
      if (this.partnerForm.valid) {
      let obj = {
        name: this.partnerForm.get('Name').value,
        contact_no: this.partnerForm.get('phone').value.toString(),
        email_id: this.partnerForm.get('email').value,
      }
      this.rest.get_intouch(obj).subscribe(
        success => {  
        if(success.status == 1){
          this.partnerForm.reset();
          
          $('#partnerModal').modal('hide');
  
          this.toastr.success(success.message);
        }
        },
        error=>{
          console.log("message");
          console.log(error);
          this.toastr.error(error.error);
  
        }
      
      );
    }
    
  }

}
