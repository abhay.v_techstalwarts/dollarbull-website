import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import { RestService } from './../service/rest.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {
  videourl:any;
  constructor(private sanitizer: DomSanitizer ,private rest: RestService ,private toastr: ToastrService) { }

  ngOnInit(): void {
    
    let data = 'About us';
    this.rest.get_video(data).subscribe(
      success => {  
      if(success.status == 1){
        console.log(success);
        this.videourl = this.sanitizer.bypassSecurityTrustResourceUrl(success.result[0].url);
      }
      },
      error=>{
        console.log("message");
        console.log(error);
        this.toastr.error(error.error);

      }
    
    );
  }

}
