import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { RestService } from '../service/rest.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  gettingToKnowDollarBull:any;
  howInvestingOnDollarbull:any;
  addingFunds:any;
  withdrawal:any;
  taxation:any;
  saftyAndSecurty:any;
  investingAndTrading:any;
  faqList:any = [];
  constructor(private restService : RestService) { }

  ngOnInit(): void {


    this.restService.get_faq().subscribe(
      success => {  
      if(success.status == 1){
        // this.partnerForm.reset();
        console.log(success.result.records);
        this.faqList = [...success.result.records];

       this.addingFunds =  this.faqList.filter((el:any) => {
         return el.section == 'Adding Funds';
        })


        this.withdrawal =  this.faqList.filter((el:any) => {
          return el.section == 'Withdrawal';
         })

         
        this.taxation =  this.faqList.filter((el:any) => {
          return el.section == 'Taxation';
         })
        
         this.saftyAndSecurty =  this.faqList.filter((el:any) => {
          return el.section == 'Safety and Security';
         })

         this.investingAndTrading =  this.faqList.filter((el:any) => {
          return el.section == 'Investing and Trading';
         })

         this.howInvestingOnDollarbull =  this.faqList.filter((el:any) => {
          return el.section == 'How Investing on DollarBull Works?';
         })

         console.log("how invest" +  this.howInvestingOnDollarbull );

         this.gettingToKnowDollarBull =  this.faqList.filter((el:any) => {
          return el.section == 'Getting to know DollarBull';
         })
        

      }
      },
      error=>{
        console.log("message");
        console.log(error);

      }
    
    );
  }

}
