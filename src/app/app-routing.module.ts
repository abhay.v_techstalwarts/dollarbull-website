import { TradestationDisclaimerComponent } from './tradestation-disclaimer/tradestation-disclaimer.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { UserAgreementComponent } from './user-agreement/user-agreement.component';
import { RefundPolicyComponent } from './refund-policy/refund-policy.component';
import { SecurityComponent } from './security/security.component';
import { AdvisorsComponent } from './advisors/advisors.component';
import { FaqComponent } from './faq/faq.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { AboutusComponent } from "./aboutus/aboutus.component";


const routes: Routes = [
  {path: '',
  redirectTo: 'home',
  pathMatch: 'full',},
  {path: 'home',
  component: HomeComponent},
  {path: 'aboutus',
  component: AboutusComponent},
  {path: 'rm',
  component: AdvisorsComponent},
  {path: 'faq',
  component: FaqComponent},
  {path: 'Safetysecurity',
  component: SecurityComponent},
  {path: 'privacypolicy',
  component: PrivacyPolicyComponent},
  {path: 'userAgreement',
  component: UserAgreementComponent},
  {path: 'refundPolicy',
  component: RefundPolicyComponent},
  {path: 'termsandcondition',
  component: TermsConditionComponent},
  {path: 'disclaimer',
  component: DisclaimerComponent},
  {path: 'traestationdisclaimer',
  component: TradestationDisclaimerComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes ,{
    scrollPositionRestoration: 'enabled', // Add options right here
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
