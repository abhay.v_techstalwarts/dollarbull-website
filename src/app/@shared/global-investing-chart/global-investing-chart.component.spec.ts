import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalInvestingChartComponent } from './global-investing-chart.component';

describe('GlobalInvestingChartComponent', () => {
  let component: GlobalInvestingChartComponent;
  let fixture: ComponentFixture<GlobalInvestingChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GlobalInvestingChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalInvestingChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
