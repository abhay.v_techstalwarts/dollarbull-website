import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
@Component({
  selector: 'app-global-investing-chart',
  templateUrl: './global-investing-chart.component.html',
  styleUrls: ['./global-investing-chart.component.scss']
})
export class GlobalInvestingChartComponent implements OnInit {
  canvas1:any;
  ctx1:any
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    // bar chart
this.canvas1 = document.getElementById("myChart1");
this.ctx1 = this.canvas1.getContext('2d');
var myChart = new Chart(this.ctx1, {
  type: 'horizontalBar',
  data: {
    labels: ["01", "02"],
    datasets: [{
      label: '# of Tomatoes',
      data: [450, 750],
      backgroundColor: [
        '#3B64BE',
        '#3B64BE',
      ],
      borderColor: [
        '#3B64BE',
        '#3B64BE',
      ],
      borderWidth: 1
    },]
  },
  options: {
    maintainAspectRatio: false,
    responsive: true,
    title: {
      display: true,
      text: "Returns Benchmark - INDIA vs USA",
      position:'bottom',
    },
    legend:{
        display: false,
      },
    scales: {
         xAxes: [{
      
            gridLines: {
               display: false
            },
            ticks: {
                autoSkip: true,
                beginAtZero:true,
                maxTicksLimit: 10
            } 
         },],
         yAxes: [{
            gridLines: {
               display: false
            },
       
         },
        ]
    }
  }
});
  }

}
