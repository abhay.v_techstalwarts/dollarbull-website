import { Component, OnInit , Input, NgZone } from '@angular/core';
import { AnimationItem } from 'lottie-web';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  
  @Input() bannerURL : string = "" 
  options : AnimationOptions = {};
  private animationItem: any ;
  @Input() bannerText : string = "" 
  
  constructor(private ngZone: NgZone) { 
    
  }


  ngOnInit(): void {

    console.log(['this.bannerURL', this.bannerURL]);
    this.options = {
      path: this.bannerURL,
      loop: false
    };
  }

  joinNow(){
    // document.getElementById("joinModal-btn")!.click();  
    window.open('https://app.dollarbull.com/signup', '_blank');
  }

}
