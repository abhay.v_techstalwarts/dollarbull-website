import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatYouPayComponent } from './what-you-pay.component';

describe('WhatYouPayComponent', () => {
  let component: WhatYouPayComponent;
  let fixture: ComponentFixture<WhatYouPayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WhatYouPayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatYouPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
