import { RestService } from './../../service/rest.service';
import { Component, OnInit ,Input} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ToastrService} from 'ngx-toastr'
declare const $: any;
import { Intercom } from 'ng-intercom';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @Input()
  enableAdvisory: boolean = false;
  betaForm:any;
  inv:any;
  waitlistData:any;
  waitlistForm:any;
  demoForm:any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  
  constructor(public intercom: Intercom ,private rest: RestService ,private fb: FormBuilder,private toastr: ToastrService) { }

  ngOnInit(): void {
  
    this.betaForm = this.fb.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    });
    
  this.waitlistForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['',Validators.required],
    email: ['',[Validators.required, Validators.email]],
    phone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    invest: ['',],
  });
  this.demoForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['',Validators.required],
    email: ['',[Validators.required, Validators.email]],
    phone: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    // phone_2: ['', [Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    businessType: ['',Validators.required],
    manageClient: ['',],

  });
  this.intercom.boot({
    app_id: "ni1j7rrl",
    custom_launcher_selector:".open-intercom",
  });
  }

  clickmsg(){
    console.log("abc");

  }
  keyPressNumbers(event:any) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
      return true;

    } else {
      return false;
      event.preventDefault();

    }
  }
  cancle(){
    this.waitlistForm.reset();
    $('#signupmodal').modal('hide');
  }

  close(){
    this.demoForm.reset();
    $('#demoModal').modal('hide');
  }

  waitlistFormSubmit(){
    if(this.waitlistForm.get('invest').value == null || this.waitlistForm.get('invest').value == ''){
      this.inv = 'unselected'
    }
    else{
      this.inv = this.waitlistForm.get('invest').value
    }
    console.log(this.waitlistForm.value);
    if (this.waitlistForm.valid) {
    let obj = {
      first_name: this.waitlistForm.get('firstName').value,
      last_name: this.waitlistForm.get('lastName').value,
      contact_no: this.waitlistForm.get('phone').value.toString(),
      email_id: this.waitlistForm.get('email').value,
      investment: this.inv,
    }
    this.rest.add_waitlist(obj).subscribe(
      success => {  
      if(success.status == 1){
        this.waitlistForm.reset();
        
        $('#signupmodal').modal('hide');

        this.toastr.success(success.message);
      }
      },
      error=>{
        console.log("message");
        console.log(error);
        this.toastr.error(error.error.message);

      }
    
    );
  }
}
betaFormreset(){
  this.demoForm.reset();
}


betaSubmit(){
  let obj ={
    email_id: this.betaForm.get('email').value,
  }

  this.rest.add_waitlist(obj).subscribe(
    success => {  
    if(success.status == 1){
      this.betaForm.reset();
      
      $('#demoModalbeta').modal('hide');

      this.toastr.success(success.message);
    }
    },
    error=>{
      console.log("message");
      console.log(error);
      this.toastr.error(error.error.message);

    }
  
  );
}
demoSubmit(){
  console.log(this.demoForm.get('firstName').value);
  if(this.demoForm.get('manageClient').value == null){
    this.demoForm.get('manageClient').value = '';
  }
  if (this.demoForm.valid) {
  let obj ={
    first_name: this.demoForm.get('firstName').value,
    last_name: this.demoForm.get('lastName').value,
    contact_no: this.demoForm.get('phone').value.toString(),
    // contact_no_2: this.demoForm.get('phone_2')?.value.toString(),
    email_id: this.demoForm.get('email').value,
    business_type: this.demoForm.get('businessType').value,
    no_of_managed_clients: this.demoForm.get('manageClient').value,

  }

  this.rest.schedule_demo(obj).subscribe(
    success => {  
    if(success.status == 1){
      $('#demoModal').modal('hide');
      // $('#myModal').modal('hide')
      this.demoForm.reset();
      // alert(success.message);
      this.toastr.success(success.message);

    }
    },
    error=>{
      console.log("message");
      console.log(error);
    }
  
  );
}
  
}

joinNow(){
  window.open('https://app.dollarbull.com/signup', '_blank');
}

}
