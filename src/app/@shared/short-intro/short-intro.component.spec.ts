import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortIntroComponent } from './short-intro.component';

describe('ShortIntroComponent', () => {
  let component: ShortIntroComponent;
  let fixture: ComponentFixture<ShortIntroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShortIntroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
