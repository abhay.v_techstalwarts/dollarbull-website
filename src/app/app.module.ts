import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { FaqComponent } from './faq/faq.component';
import { FooterComponent } from "./@shared/footer/footer.component";
import { HeaderComponent } from "./@shared/header/header.component";
import { BlogComponent } from './@shared/blog/blog.component';
import { TestimonialsComponent } from './@shared/testimonials/testimonials.component';
import { BannerComponent } from './@shared/banner/banner.component';
import { ShortIntroComponent } from './@shared/short-intro/short-intro.component';
import { AdvisorsComponent } from './advisors/advisors.component';
import { GlobalInvestingChartComponent } from './@shared/global-investing-chart/global-investing-chart.component';
import { WhatYouPayComponent } from './@shared/what-you-pay/what-you-pay.component';
import { SecurityComponent } from './security/security.component';
import { LayoutComponent } from './@shared/layout/layout.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { UserAgreementComponent } from './user-agreement/user-agreement.component';
import { RefundPolicyComponent } from './refund-policy/refund-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { TradestationDisclaimerComponent } from './tradestation-disclaimer/tradestation-disclaimer.component';
import { IntercomModule } from 'ng-intercom';

export function playerFactory() {
  return player;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    FaqComponent,
    HeaderComponent,
    FooterComponent,
    BlogComponent,
    TestimonialsComponent,
    BannerComponent,
    ShortIntroComponent,
    AdvisorsComponent,
    GlobalInvestingChartComponent,
    WhatYouPayComponent,
    SecurityComponent,
    LayoutComponent,
    PrivacyPolicyComponent,
    UserAgreementComponent,
    RefundPolicyComponent,
    TermsConditionComponent,
    DisclaimerComponent,
    TradestationDisclaimerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),BrowserAnimationsModule,
    LottieModule.forRoot({ player: playerFactory }),
    IntercomModule.forRoot({
      appId: "ni1j7rrl", // from your Intercom config
      updateOnRouterChange: true // will automatically run `update` on router event changes. Default: `false`
    })
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent],
  exports: [LayoutComponent, TermsConditionComponent, DisclaimerComponent, TradestationDisclaimerComponent]
})
export class AppModule { }
