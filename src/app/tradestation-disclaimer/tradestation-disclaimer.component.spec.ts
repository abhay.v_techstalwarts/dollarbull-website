import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradestationDisclaimerComponent } from './tradestation-disclaimer.component';

describe('TradestationDisclaimerComponent', () => {
  let component: TradestationDisclaimerComponent;
  let fixture: ComponentFixture<TradestationDisclaimerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradestationDisclaimerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradestationDisclaimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
