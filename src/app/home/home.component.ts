import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { ChartOptions } from 'chart.js'
declare const $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  canvas: any;
  position_c="bottom";
  amazonCanvas: any;
  amazonCtx: any;
  ctx: any;
  DEFTY: any;
  SNP: any;
  NFLX: any;
  AMAZON: any;
  ALPHABET: any;
  FACEBOOK:any;
  APPLE: any;
  TESLA: any;

  labels:any;


  dataDEFTY:any;
  dataSNP:any;
  dataNFLX:any;
  dataALPHABET:any;
  dataAPPLE:any;
  dataTESLA:any;
  dataAMAZON:any;
  dataFACEBOOK:any;

  //
  ALPHABETctx: any;
  ALPHABETcanvas: any;

  // 

  APPLEcanvas:any;
  APPLEctx:any;

  // 
  TESLAcanvas:any;
  TESLActx:any;

  lineChart:any;
  index:any;
  key:any;
  keyval:any = 1;
  investStep:any;
  chartTabsImages=[
    {id:0,img:'assets/img/S&P500.svg' ,s:"done"},
    {id:1,img:'assets/img/netflix.svg' ,s:""},
    {id:2,img:'assets/img/Alphabet-1.svg' ,s:""},
    {id:3,img:'assets/img/apple.svg' ,s:"done"},
    {id:4,img:'assets/img/Tesla.svg' ,s:""},
    {id:5,img:'assets/img/Amazon.svg' ,s:"done"},
    {id:6,img:'assets/img/Facebook.svg' ,s:""},


  ];
  slicedata= [];

  chartValue:any;

  selectedIndex: number=0;
  autoClickIndex:number =0;
  autoClickId:number =0;
  constructor() {}

  ngOnInit(): void {

    this.investStep =[
      { id:1 ,img:'../../assets/img/step1.png' , title:'Easy Investing' ,text:'Invest in Stocks and ETFs, listed in the US right from your DollarBull dashboard' },
      {id:2, img:'../../assets/img/step2.png' , title:'Fast and Reliable',text:'Safe and secure order executions done swiftly'},
      {id:3, img:'../../assets/img/step3.png' , title:'Accessible Anywhere' , text:'Invest on the go with our mobile app'},
      {id:4, img:'../../assets/img/step4.png' ,title:'ZERO Fees Required ' , text:'Buy & Sell Comission free'}
    ]



    this.NFLX = [ 
       100.00, 
      107.78, 
      86.58, 
      88.85, 
      89.04, 
      123.42, 
      120.73, //12 
      146.16, 
      261.80, 
      300.83, 
      289.80, 
      338.22, 
      352.61, 
      356.28, 
      395.74, 
      459.09, 
      515.68, 
      523.07, 
      578.11, //13
      576.36, 
      642.38, 
      707.85, 
      579.33, 
      534.58, 
      670.47, 
      751.55, 
      675.77, 
      757.11, 
      697.06, 
      617.01, 
      542.99, //14
      554.31, 
      700.67, 
      762.91, 
      656.27, 
      884.88, 
      989.71, 
      1041.22, 
      1251.66, 
      1176.38, 
      1178.49, 
      1196.95, 
      1394.11, //15
      1222.75, 
      1046.27, 
      1093.09, 
      1175.38, 
      1035.38, 
      1128.78, 
      1074.96, 
      1049.39, 
      1082.86, 
      1141.24, 
      1371.09, 
      1303.48, //16
      1417.68, 
      1565.46, 
      1586.26, 
      1633.74, 
      1727.48, 
      1812.44, 
      1625.40, 
      2024.16, 
      1943.10, 
      1968.34, 
      2201.75, 
      2077.43, //17
      2235.89, 
      2947.56, 
      3229.12, 
      3116.81, 
      3483.88, 
      4002.40, 
      4427.74, 
      3762.76, 
      4043.21, 
      4241.48, 
      3529.25, 
      3228.12, //18
      2976.36, 
      3779.11, 
      3973.38, 
      4080.57, 
      4212.34, 
      3743.30, 
      4165.53, 
      3552.82, 
      3216.89, 
      2997.71, 
      3189.31, 
      3447.07, //19
      3667.47, 
      3980.94, 
      4237.25, 
      4048.55, 
      4617.78, 
      4736.20, 
      5400.29, 
      5544.62, 
      6188.80, 
      5865.88, 
      5383.38, 
      5610.90,//20
     ]

    this.DEFTY = 
    [100.00,
      109.88,
      109.17,
      109.52,
      125.78,
      121.42,
      124.44,
      125.51,
      130.33,
      120.76,
      121.30,
      128.01,
      121.42,
      115.34,
      109.06,
      97.48,
      107.21,
      117.84,
      115.57,
      117.68,
      110.73,
      116.32,
      129.34,
      128.57,
      143.99,
      146.83,
      144.50,
      153.53,
      148.82,
      156.77,
      159.25,
      151.31,
      164.42,
      167.56,
      158.66,
      151.57,
      153.34,
      153.67,
      154.48,
      135.90,
      140.28,
      142.20,
      138.32,
      139.17,
      129.14,
      122.56,
      134.49,
      136.08,
      140.47,
      142.83,
      149.67,
      151.58,
      151.90,
      149.55,
      138.59,
      139.08,
      149.03,
      154.78,
      164.27,
      167.74,
      172.32,
      171.46,
      182.58,
      180.19,
      174.14,
      186.95,
      181.56,
      189.10,
      199.57,
      185.45,
      181.09,
      185.94,
      184.40,
      179.32,
      191.70,
      188.50,
      174.73,
      163.26,
      178.88,
      178.01,
      176.74,
      177.30,
      194.82,
      195.51,
      201.85,
      199.03,
      183.89,
      172.52,
      184.88,
      194.19,
      194.45,
      199.04,
      189.02,
      177.06,
      126.19,
      141.96,
      150.44,
      159.57,
      167.93,
      182.04,
      180.52,
      181.34,
      205.82,
    ]

    this.SNP = [  
      100.00, 
      107.01, 
      107.92, 
      110.54, 
      113.83, 
      112.66, 
      111.55, 
      115.99, 
      120.15, 
      120.85, 
      124.54, 
      126.32, 
      131.27, 
      129.44, 
      136.99, 
      131.92, 
      136.58, 
      142.12, 
      145.64, 
      148.42, 
      141.25, 
      150.06, 
      153.52, 
      153.55, 
      157.29, 
      161.51, 
      157.75, 
      164.45, 
      160.09, 
      166.17, 
      169.49, 
      170.19, 
      167.26, 
      175.68, 
      171.18, 
      175.39, 
      176.10, 
      173.56, 
      175.44, 
      160.43, 
      161.55, 
      176.90, 
      177.23, 
      169.98, 
      163.95, 
      167.71, 
      176.01, 
      176.96, 
      178.97, 
      179.58, 
      185.57, 
      186.06, 
      185.52, 
      181.47, 
      188.82, 
      194.86, 
      196.99, 
      207.54, 
      204.61, 
      207.40, 
      211.57, 
      211.78, 
      216.14, 
      216.72, 
      221.59, 
      226.24, 
      232.35, 
      237.37, 
      248.77, 
      236.55, 
      228.40, 
      235.11, 
      242.79, 
      242.39, 
      250.37, 
      258.39, 
      261.22, 
      245.05, 
      250.13, 
      225.37, 
      243.37, 
      252.67, 
      258.76, 
      264.16, 
      248.56, 
      268.86, 
      268.24, 
      264.54, 
      268.03, 
      279.92, 
      284.81, 
      298.47, 
      297.99, 
      283.99, 
      227.45, 
      260.95, 
      282.31, 
      288.28, 
      305.19, 
      327.24, 
      314.13, 
      307.89, 
      341.24,
     ]

    this.ALPHABET = [ 
       100.00, 
      110.13, 
      111.46, 
      119.05, 
      121.03, 
      111.49, 
      120.22, 
      123.58, 
      127.27, 
      127.62, 
      125.64, 
      119.22, 
      128.17, 
      135.48, 
      146.75, 
      138.70, 
      154.14, 
      172.41, 
      188.41, 
      191.13, 
      166.24, 
      172.79, 
      164.72, 
      147.87, 
      148.32, 
      159.63, 
      147.47, 
      164.43, 
      152.46, 
      146.83, 
      156.57, 
      148.17, 
      175.04, 
      185.22, 
      177.82, 
      203.09, 
      206.95, 
      210.06, 
      256.95, 
      238.47, 
      250.08, 
      301.77, 
      326.13, 
      305.92, 
      276.06, 
      278.09, 
      287.44, 
      328.43, 
      345.52, 
      348.52, 
      368.72, 
      370.10, 
      401.85, 
      377.20, 
      357.15, 
      361.96, 
      399.75, 
      409.70, 
      428.16, 
      455.40, 
      478.32, 
      458.01, 
      478.43, 
      469.82, 
      460.66, 
      530.05, 
      558.23, 
      571.04, 
      667.56, 
      717.25, 
      658.91, 
      759.90, 
      788.37, 
      823.06, 
      863.11, 
      979.50, 
      962.62, 
      799.89, 
      851.20, 
      739.18, 
      781.02, 
      802.87, 
      871.29, 
      918.03, 
      812.93, 
      923.15, 
      891.04, 
      859.59, 
      833.57, 
      860.36, 
      855.63, 
      911.54, 
      962.54, 
      938.41, 
      916.19, 
      1097.90, 
      1186.74, 
      1382.53, 
      1494.52, 
      1680.49, 
      1547.05, 
      1442.94, 
      1546.48, 
    ]

    this.APPLE =[
        100.00, 
      105.62, 
      108.17, 
      120.83, 
      118.05, 
      106.79, 
      105.42, 
      98.74, 
      81.58, 
      77.87, 
      77.58, 
      79.46, 
      82.07, 
      74.51, 
      83.15, 
      89.55, 
      89.44, 
      95.31, 
      101.62, 
      101.97, 
      92.46, 
      97.88, 
      100.45, 
      109.70, 
      117.24, 
      122.09, 
      125.50, 
      135.53, 
      130.12, 
      143.53, 
      151.62, 
      144.06, 
      156.31, 
      170.77, 
      164.37, 
      170.58, 
      173.41, 
      168.17, 
      157.33, 
      143.74, 
      146.22, 
      161.70, 
      157.25, 
      141.18, 
      129.23, 
      135.45, 
      148.20, 
      126.17, 
      133.47, 
      129.99, 
      143.76, 
      145.47, 
      153.36, 
      151.95, 
      149.99, 
      159.12, 
      176.38, 
      192.33, 
      197.71, 
      201.67, 
      211.62, 
      198.25, 
      207.30, 
      227.53, 
      213.33, 
      231.47, 
      238.09, 
      239.78, 
      233.54, 
      244.59, 
      232.96, 
      236.34, 
      266.91, 
      262.62, 
      282.71, 
      321.52, 
      319.97, 
      312.87, 
      261.12, 
      223.12, 
      235.27, 
      248.27, 
      271.35, 
      298.71, 
      246.84, 
      287.08, 
      296.88, 
      294.11, 
      321.12, 
      365.77, 
      378.83, 
      430.73, 
      442.64, 
      429.53, 
      346.30, 
      415.53, 
      463.91, 
      524.82, 
      628.08, 
      775.01, 
      674.57, 
      628.24, 
      710.04,
     ]

    this.TESLA = [
      100.00, 
      107.99, 
      93.25, 
      99.96, 
      103.59, 
      103.91, 
      122.98, 
      125.61, 
      136.06, 
      123.09, 
      156.06, 
      189.27, 
      328.92, 
      416.27, 
      481.53, 
      600.14, 
      685.61, 
      576.09, 
      441.10, 
      533.21, 
      629.17, 
      890.09, 
      770.76, 
      737.94, 
      727.18, 
      851.58, 
      828.67, 
      1009.31, 
      853.43, 
      861.78, 
      822.88, 
      779.08, 
      749.34, 
      700.99, 
      666.39, 
      802.95, 
      886.15, 
      956.13, 
      923.59, 
      847.71, 
      852.15, 
      759.47, 
      842.59, 
      793.64, 
      699.61, 
      661.99, 
      844.01, 
      858.97, 
      779.96, 
      769.09, 
      817.09, 
      713.21, 
      759.15, 
      677.76, 
      646.11, 
      770.83, 
      885.40, 
      888.17, 
      1060.46, 
      1146.82, 
      1209.13, 
      1252.65, 
      1135.24, 
      1262.52, 
      1213.25, 
      1140.60, 
      1088.92, 
      1138.65, 
      1240.67, 
      1175.59, 
      896.91, 
      1065.44, 
      1036.66, 
      1190.30, 
      1068.70, 
      1026.47, 
      1103.73, 
      1223.02, 
      1273.50, 
      1101.67, 
      1109.09, 
      1047.21, 
      1027.28, 
      831.30, 
      635.77, 
      807.00, 
      830.73, 
      799.33, 
      869.24, 
      1113.00, 
      1189.59, 
      1528.45, 
      2770.87, 
      2641.63, 
      1710.69, 
      2491.37, 
      3190.41, 
      3977.37, 
      5275.31, 
      8437.83, 
      7960.21, 
      7113.85, 
      10386.50, 
    ]

    this.AMAZON=[ 
       100.00, 
      110.13, 
      111.46, 
      119.05, 
      121.03, 
      111.49, 
      120.22, 
      123.58, 
      127.27, 
      127.62, 
      125.64, 
      119.22, 
      128.17, 
      135.48, 
      146.75, 
      138.70, 
      154.14, 
      172.41, 
      188.41, 
      191.13, 
      166.24, 
      172.79, 
      164.72, 
      147.87, 
      148.32, 
      159.63, 
      147.47, 
      164.43, 
      152.46, 
      146.83, 
      156.57, 
      148.17, 
      175.04, 
      185.22, 
      177.82, 
      203.09, 
      206.95, 
      210.06, 
      256.95, 
      238.47, 
      250.08, 
      301.77, 
      326.13, 
      305.92, 
      276.06, 
      278.09, 
      287.44, 
      328.43, 
      345.52, 
      348.52, 
      368.72, 
      370.10, 
      401.85, 
      377.20, 
      357.15, 
      361.96, 
      399.75, 
      409.70, 
      428.16, 
      455.40, 
      478.32, 
      458.01, 
      478.43, 
      469.82, 
      460.66, 
      530.05, 
      558.23, 
      571.04, 
      667.56, 
      717.25, 
      658.91, 
      759.90, 
      788.37, 
      823.06, 
      863.11, 
      979.50, 
      962.62, 
      799.89, 
      851.20, 
      739.18, 
      781.02, 
      802.87, 
      871.29, 
      918.03, 
      812.93, 
      923.15, 
      891.04, 
      859.59, 
      833.57, 
      860.36, 
      855.63, 
      911.54, 
      962.54, 
      938.41, 
      916.19, 
      1097.90, 
      1186.74, 
      1382.53, 
      1494.52, 
      1680.49, 
      1547.05, 
      1442.94, 
      1546.48,
     ]

    this.FACEBOOK=[ 
       100.00, 
      111.00, 
      75.32, 
      63.96, 
      79.33, 
      76.52, 
      97.55, 
      101.01, 
      107.25, 
      100.22, 
      92.10, 
      98.95, 
      86.04, 
      89.50, 
      135.25, 
      151.05, 
      181.89, 
      179.47, 
      169.77, 
      197.37, 
      221.79, 
      243.18, 
      225.90, 
      220.60, 
      227.56, 
      245.53, 
      261.04, 
      276.62, 
      276.15, 
      266.52, 
      270.92, 
      283.01, 
      270.53, 
      287.70, 
      294.62, 
      284.96, 
      289.65, 
      313.53, 
      339.61, 
      314.68, 
      328.10, 
      372.69, 
      386.44, 
      368.76, 
      415.19, 
      396.18, 
      418.69, 
      427.74, 
      428.50, 
      411.94, 
      448.45, 
      455.16, 
      464.54, 
      467.17, 
      415.22, 
      421.57, 
      480.63, 
      495.74, 
      513.28, 
      550.00, 
      546.65, 
      535.46, 
      612.77, 
      620.56, 
      611.36, 
      658.95, 
      631.67, 
      654.47, 
      696.57, 
      634.70, 
      560.57, 
      627.20, 
      699.82, 
      711.98, 
      619.23, 
      617.46, 
      586.00, 
      547.44, 
      508.98, 
      489.47, 
      597.80, 
      585.43, 
      608.59, 
      696.36, 
      592.17, 
      696.25, 
      695.27, 
      657.97, 
      634.24, 
      698.48, 
      720.42, 
      756.78, 
      736.62, 
      708.66, 
      575.76, 
      729.69, 
      836.62, 
      856.96, 
      908.95, 
      1065.80, 
      961.87, 
      942.86, 
      1033.73,
     ]

    this.labels =[
      '2012','2012','2012','2012','2012','2012','2012',

    ,'2013', '2013','2013','2013','2013','2013','2013','2013','2013','2013','2013','2013',

    '2014', '2014','2014','2014','2014','2014','2014','2014','2014','2014','2014','2014',
    
    '2015','2015','2015','2015','2015','2015','2015','2015','2015','2015','2015','2015',
    
    '2016','2016','2016','2016','2016','2016','2016','2016','2016','2016','2016','2016',

    '2017', '2017','2017','2017','2017','2017','2017','2017','2017','2017','2017','2017',
    
    '2018','2018','2018','2018','2018','2018','2018','2018','2018','2018','2018','2018',
    
    '2019','2019','2019','2019','2019','2019','2019','2019','2019','2019','2019','2019',
    
    '2020','2020','2020','2020','2020','2020','2020','2020','2020','2020','2020','2020',
  ]





    //  CHARTS

    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');
    

     this.dataDEFTY = {
      label: 'Nifty 50 (Dollar adjusted)',
      data: this.DEFTY,
      lineTension: 0,
      fill: false,
      backgroundColor: '#9d480d',
      pointRadius: 0,
      borderColor: '#9d480d',
      pointStyle: 'rect',
    };

     this.dataSNP = {
      label: 'S&P 500',
      data: this.SNP,
      lineTension: 0,
      fill: false,
      pointBorderWidth:0,
      backgroundColor: '#254378',
      pointRadius: 0,
      borderColor: '#254378',
      pointStyle: 'rect',
      borderHeight:20,
    };

     this.dataNFLX = {
      label: 'NFLX',
      data: this.NFLX,
      lineTension: 0,
      fill: false,
      pointBorderWidth:0,
      backgroundColor: '#3d76cd',
      pointRadius: 0,
      borderColor: '#3d76cd',
      pointStyle: 'rect',
    };

     this.dataALPHABET = {
      label: 'ALPHABET',
      data: this.ALPHABET,
      lineTension: 0,
      fill: false,
      backgroundColor: '#a5a5a5',
      pointRadius: 0,
      borderColor: '#a5a5a5',
      pointStyle: 'rect',
    };

     this.dataAPPLE = {
      label: 'APPLE',
      data: this.APPLE,
      lineTension: 0,
      fill: false,
      backgroundColor: '#5b9bd5',
      pointRadius: 0,
      borderColor: '#5b9bd5',
      pointStyle: 'rect',
    };
     this.dataTESLA = {
      label: 'TESLA',
      data: this.TESLA,
      lineTension: 0,
      fill: false,
      backgroundColor: '#70ad47',
      pointRadius: 0,
      borderColor: '#70ad47',
      pointStyle: 'rect',
    };

     this.dataAMAZON = {
      label: 'AMAZON',
      data: this.AMAZON,
      lineTension: 0,
      fill: false,
      backgroundColor: '#ed7d31',
      pointRadius: 0,
      borderColor: '#ed7d31',
      pointStyle: 'rect',
    };

    
     this.dataFACEBOOK = {
      label: 'FACEBOOK',
      data: this.FACEBOOK,
      lineTension: 0,
      fill: false,
      backgroundColor: '#ffca29',
      pointRadius: 0,
      borderColor: '#ffca29',
      pointStyle: 'rect',
    };

    // var speedData = {
    //   labels: this.labels,
    //   datasets: [dataDEFTY, dataSNP, dataNFLX, dataALPHABET, dataAPPLE, dataTESLA, dataAMAZON, dataFACEBOOK],
    // };

    this.chartValue =[this.dataDEFTY, this.dataSNP];
    this.lineChart = new Chart(this.ctx, {
      type: 'line',
      data: { 
      labels: this.labels,
      datasets: this.chartValue,
       },
      options: {
      responsive: true,
      maintainAspectRatio: true,
      legend: {
        display: true,
        position:'bottom',
        labels: {
          usePointStyle: true,
          fontColor: 'black',
          boxWidth: 8,
          padding: 20,
        },
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              display: false,
            },
            ticks: {
              autoSkip: true,
              maxTicksLimit: 10,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            gridLines: {
              display: true,
              drawBorder: false,
            },
          },
        ],
      },
    }
    });

   var clearIntervalid =  setInterval(() => {
      this.autoClickId++;
      this.autoClickIndex++;
      this.chartTabsClick(this.autoClickIndex ,this.autoClickId); 
      if(this.autoClickId == 6){
        clearInterval(clearIntervalid);
      }
      }, 2000);

    

  }

  ngAfterViewInit() {
  }
    joinNow(){
      // document.getElementById("joinModal-btn")!.click();
      window.open('https://app.dollarbull.com/signup', '_blank');
    }

    chartTabsClick( index:any, id:any){
      this.selectedIndex = index;
      // snp
      if(id == 0){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataSNP);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // netflix
      if(id == 1){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataNFLX);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // alphabet
      if(id == 2){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataALPHABET);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // apple
      if(id == 3){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataAPPLE);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // tesla
      if(id == 4){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataTESLA);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // amazon
      if(id == 5){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);

        //@ts-ignore
        this.chartValue.push(this.dataAMAZON);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }
      // facebook
      if(id == 6){
        this.slicedata = this.chartValue.pop();
        console.log(this.chartValue);
        //@ts-ignore
        this.chartValue.push(this.dataFACEBOOK);
        console.log(this.chartValue);

        this.lineChart.data.datasets= this.chartValue;
        this.lineChart.update();
      }

    }

  
}
